package com.flightmanagementsystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.flightmanagementsystem.dao.AirportRepository;
import com.flightmanagementsystem.dao.FlightRepository;
import com.flightmanagementsystem.model.ScheduleFlight;
import com.flightmanagementsystem.service.ScheduledFlightService;

@RestController
@RequestMapping("/api/v1")
public class ScheduledFlightController {
	
	@Autowired
	ScheduledFlightService serviceImpl;
	

	@Autowired
	AirportRepository airportRepo;

	@Autowired
	FlightRepository flightRepo;
	
	
	
	//view scheduled flight
	@GetMapping("/scheduledFlight/{id}")
	public ResponseEntity<ScheduleFlight> viewFlightbyId(@PathVariable long id)
	{	ScheduleFlight sf= serviceImpl.viewScheduledFlight(id);
		if(sf!=null) {
		return new ResponseEntity<ScheduleFlight>(sf,HttpStatus.OK);
		}
			return new ResponseEntity("",HttpStatus.NOT_FOUND);
		
		
	}
	
	//modify a flight
	@PutMapping("/modifyScheduledFlight")
	public ResponseEntity<ScheduleFlight> modify(@RequestBody ScheduleFlight sf) {
		ScheduleFlight modifySf = serviceImpl.modifyScheduledFlight(sf);
		if(modifySf==null) {
			return new ResponseEntity("can not modify",HttpStatus.NOT_MODIFIED);
		}
		
		return new ResponseEntity("modified : "+modifySf,HttpStatus.OK);
		
	}
	
	//remove a flight
	@DeleteMapping("/deleteFlight/{id}")
	public String deleteFlight(@RequestParam long id) {
		return serviceImpl.removeScheduledFlights(id);
		
	}
	
	//show all scheduled flight
	@GetMapping("/scheduledFlights")
	public Iterable<ScheduleFlight> viewAllSf(){
	return serviceImpl.viewAllScheduledFlight();
	}
	

	
	
}
