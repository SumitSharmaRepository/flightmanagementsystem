package com.flightmanagementsystem.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flightmanagementsystem.dao.AirportRepository;
import com.flightmanagementsystem.exceptions.RecordNotFoundException;
import com.flightmanagementsystem.model.Airport;

@RestController
@RequestMapping("/api/v1")
public class AirportController {
	
	@Autowired
	AirportRepository airportRepo;
	
	
	//view all airport
	@GetMapping("/viewairport")
	public List<Airport> viewAllAirport() {
		return airportRepo.findAll();
		
	}
	
	//get airport by id
	@GetMapping("/getAirport/{code}")
	public ResponseEntity<Airport> getAirportDetailById(@PathVariable Long code) {
		
		Airport airportDetail= airportRepo.findById(code).orElseThrow(()-> new 
				RecordNotFoundException("Airport Id record doesnot exist"));
		
		return ResponseEntity.ok(airportDetail);
	}
	
	//save a airport
	@PostMapping("/addAirport")
	public Airport addNewAirport(@RequestBody Airport airport) {
		return airportRepo.save(airport);
	}
	
	// update a user
	@PutMapping("/updateAirport/{code}")
	public ResponseEntity<Airport> updateAirportDetails(@PathVariable Long code, @RequestBody Airport airport ) {
		Airport airportDetail= airportRepo.findById(code).orElseThrow(()-> new 
				RecordNotFoundException("Airport Id record doesnot exist"));
		airportDetail.getAiportName();
		airportDetail.getAirportLocation();
		
		
		Airport updatedAirport = airportRepo.save(airportDetail);

		return ResponseEntity.ok(updatedAirport);
		
	}
	
	//delete a airport
	@DeleteMapping("/deleteAirport/{code}")
	public ResponseEntity<Map<String, Boolean>> deleteAirport(@PathVariable Long code)
	{
		Map<String,Boolean> response = new HashMap<>();
		Airport airportDetail= airportRepo.findById(code).orElseThrow(()-> new 
				RecordNotFoundException("Airport Id record doesnot exist"));
		airportRepo.delete(airportDetail);
		response.put("deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);
		
	}
	
	
	
}
