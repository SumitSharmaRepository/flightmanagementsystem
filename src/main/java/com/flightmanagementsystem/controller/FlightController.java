package com.flightmanagementsystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flightmanagementsystem.model.Flight;
import com.flightmanagementsystem.service.FlightService;
import com.flightmanagementsystem.serviceImpl.FlightServiceImpl;

@RestController
@RequestMapping("/api/v1")
public class FlightController {
	
	@Autowired
	FlightService flightServiceImpl;
	
	//add flight
	@PostMapping("/addFlight")
	public void addFlight(@RequestBody Flight flight)
	{
		flightServiceImpl.addFlight(flight);
	}
	
	//view all flight details
	@GetMapping("/flight")
	public void getAllFlightDetails() {
		flightServiceImpl.getFlightView();
	}
	
	//get flight details
	@GetMapping("/flight/{flightNumber}")
	public void getFlightDetails(@PathVariable Long flightNumber) {
		flightServiceImpl.getFlightDetail(flightNumber);
	}
	
	
	//modify a flight
	@PutMapping("/flight/{flightNumber}")
	public void modifyFlightDetails(@RequestBody Flight flight) {
		flightServiceImpl.modifyFlight(flight);
		
	}
	@DeleteMapping("/deleteFlight/{flightNumber}")
	public void removeFlight(@PathVariable("id") Long flightNumber) {
		flightServiceImpl.removeFlight(flightNumber);
	}
	
}
