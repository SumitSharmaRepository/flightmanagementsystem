package com.flightmanagementsystem.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flightmanagementsystem.dao.UserRepository;
import com.flightmanagementsystem.exceptions.RecordNotFoundException;
import com.flightmanagementsystem.model.User;

@RestController
@RequestMapping("/api/v1")
public class UserController {

	@Autowired
	private UserRepository userRepository;

	@GetMapping("/users")
	public List<User> getAllUserDetails() {
		return userRepository.findAll();
	}

	@GetMapping("/users/{id}")
	public ResponseEntity<User> getAllUserDetailsById(@PathVariable Long id) {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new RecordNotFoundException("this user record is not found in our databases" + id));
		return ResponseEntity.ok(user);
	}

	@PostMapping("/user")
	public User createUser(@RequestBody User user) {
		return userRepository.save(user);
	}

	// update user via rest api
	@PutMapping("/users/{id}")
	public ResponseEntity<User> updateuser(@PathVariable Long id, @RequestBody User userDetails) {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new RecordNotFoundException("this user record is not found in our databases" + id));

		user.setUserEmail(userDetails.getUserEmail());
		user.setUserPassword(userDetails.getUserPassword());
		user.setUserType(userDetails.getUserType());
		user.setUserPhone(userDetails.getUserPhone());

		User updatedUser = userRepository.save(user);
		return ResponseEntity.ok(updatedUser);
	}

	// Delete a User
	@DeleteMapping("/users/{id}")
	public ResponseEntity<Map<String, Boolean>> deleteUserById(@PathVariable Long id) {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new RecordNotFoundException("this user record is not found in our databases" + id));

		userRepository.delete(user);

		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);

	}
}
