package com.flightmanagementsystem.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.flightmanagementsystem.model.Flight;


public interface FlightService {

	
	public ResponseEntity<Flight> addFlight(Flight flight);
	
	public Iterable<Flight> getFlightView();
	
	public Flight getFlightDetail(Long flightNumber);

	Flight modifyFlight(Flight flight);

	public String removeFlight(Long flightNumber);
}
