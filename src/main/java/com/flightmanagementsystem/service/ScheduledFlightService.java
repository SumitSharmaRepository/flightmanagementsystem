package com.flightmanagementsystem.service;

import com.flightmanagementsystem.model.ScheduleFlight;

public interface ScheduledFlightService {
	
	public Iterable<ScheduleFlight> viewAllScheduledFlight();
	
	public ScheduleFlight modifyScheduledFlight(ScheduleFlight scheduleFlight);
	
	public String removeScheduledFlights(long id);
	
	public ScheduleFlight addScheduleFlight(ScheduleFlight scheduleFlight );
	
	public ScheduleFlight viewScheduledFlight(long id);
	
}
