package com.flightmanagementsystem.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.flightmanagementsystem.model.Airport;

@Repository
public interface AirportRepository extends JpaRepository<Airport, Long>{

}
