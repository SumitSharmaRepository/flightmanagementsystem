package com.flightmanagementsystem.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.flightmanagementsystem.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
