package com.flightmanagementsystem.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.flightmanagementsystem.model.Booking;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Long>{

}
