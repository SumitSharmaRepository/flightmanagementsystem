package com.flightmanagementsystem.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.flightmanagementsystem.model.Schedule;

@Repository
public interface ScheduleRepository extends CrudRepository<Schedule, Long>{

}
