package com.flightmanagementsystem.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.flightmanagementsystem.model.ScheduleFlight;

@Repository
public interface ScheduledFlightRepository extends JpaRepository<ScheduleFlight, Long>{

}
