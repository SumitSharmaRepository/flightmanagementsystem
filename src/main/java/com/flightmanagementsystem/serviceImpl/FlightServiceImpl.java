package com.flightmanagementsystem.serviceImpl;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.flightmanagementsystem.dao.FlightRepository;
import com.flightmanagementsystem.exceptions.RecordNotFoundException;
import com.flightmanagementsystem.model.Flight;
import com.flightmanagementsystem.service.FlightService;

@Service
public class FlightServiceImpl implements FlightService {

	@Autowired
	FlightRepository repo;
	
	@Override
	public ResponseEntity<Flight> addFlight(Flight flight) {
		// TODO Auto-generated method stub
		
		Optional<Flight> checkPresentFlight = repo.findById(flight.getFlightNumber());
		
		if(!checkPresentFlight.isPresent()) {
			repo.save(flight);
			return new ResponseEntity<Flight>(flight,HttpStatus.OK);
			
		}
		
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@Override
	public Iterable<Flight> getFlightView() {
		// TODO Auto-generated method stub
		return repo.findAll();
		
		
	}
	
	@Override
	public Flight getFlightDetail(Long flightNumber) {
		// TODO Auto-generated method stub

		Flight flightPresent = repo.findById(flightNumber).orElseThrow(
				() -> new RecordNotFoundException("this user record is not found in our databases" + flightNumber));

		return flightPresent;

	}
	
	@Override
	public Flight modifyFlight(Flight flight ){
		Optional<Flight> findById = repo.findById(flight.getFlightNumber());
		if (findById.isPresent()) {
			repo.save(flight);
		} else
			throw new RecordNotFoundException("Flight with number: " + flight.getFlightNumber() + " not exists");
		return flight;
		
		
		
	}

	/*
	 * remove a flight
	 */
	public String removeFlight(Long flightNumber) {
		Optional<Flight> findById = repo.findById(flightNumber);
		if (findById.isPresent()) {
			repo.deleteById(flightNumber);
			return "Flight removed!!";
		} else
			throw new RecordNotFoundException("Flight with number: " + flightNumber + " not exists");

	}
	
	
}
