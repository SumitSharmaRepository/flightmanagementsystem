package com.flightmanagementsystem.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flightmanagementsystem.dao.BookingRepository;
import com.flightmanagementsystem.dao.ScheduleRepository;
import com.flightmanagementsystem.dao.ScheduledFlightRepository;
import com.flightmanagementsystem.exceptions.RecordNotFoundException;
import com.flightmanagementsystem.model.Schedule;
import com.flightmanagementsystem.model.ScheduleFlight;
import com.flightmanagementsystem.service.ScheduledFlightService;

@Service
public class ScheduledFlightServiceImpl implements ScheduledFlightService {

	@Autowired
	ScheduledFlightRepository schFlightRepo;

	@Autowired
	ScheduleRepository schRepo;

	@Autowired
	BookingRepository bookingRepo;

	@Override
	public Iterable<ScheduleFlight> viewAllScheduledFlight() {
		// TODO Auto-generated method stub

		return schFlightRepo.findAll();

	}

	@Override
	public ScheduleFlight modifyScheduledFlight(ScheduleFlight scheduleFlight) {
		// TODO Auto-generated method stub
		ScheduleFlight updateScheduledFlight = schFlightRepo.findById(scheduleFlight.getId()).get();
		Schedule updateSchedule = (Schedule) schRepo.findById(scheduleFlight.getSchedule().getScheduledId()).get();
		updateScheduledFlight.setAvailableSeats(scheduleFlight.getAvailableSeats());
		updateSchedule.setDstnAirport(scheduleFlight.getSchedule().getDstnAirport());
		updateSchedule.setSrcAirport(scheduleFlight.getSchedule().getSrcAirport());
		updateSchedule.setArrDateTime(scheduleFlight.getSchedule().getArrDateTime());
		schFlightRepo.save(updateScheduledFlight);

		return scheduleFlight;
	}

	@Override
	public String removeScheduledFlights(long id) {
		// TODO Auto-generated method stub
		ScheduleFlight sf = schFlightRepo.findById(id).orElseThrow(() -> new RecordNotFoundException("Not Found"));
		schFlightRepo.deleteById(id);
		return "flight Deleted : " + id;
	}

	@Override
	public ScheduleFlight addScheduleFlight(ScheduleFlight scheduleFlight) {
		// TODO Auto-generated method stub
		return schFlightRepo.save(scheduleFlight);

	}

	@Override
	public ScheduleFlight viewScheduledFlight(long id) {
		// TODO Auto-generated method stub
		ScheduleFlight showTheDetails=schFlightRepo.findById(id).orElseThrow(() -> new RecordNotFoundException("Not Found"));;
		
		return showTheDetails;
	}

}
