package com.flightmanagementsystem.model;

import java.sql.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name= "schedule_table")
public class Schedule {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="scheduled_id")
	Long scheduledId;
	
	@Column(name="arrival_DateTime")
	Date arrDateTime;
	
	@Column(name="departure_DateTime")
	Date deptDateTime;
	
	@Column(name="destination_airport")
	String dstnAirport;
	
	@Column(name="other")
	String other;
	
	@Column(name="prime")
	boolean prime;
	
	@Column(name="result")
	String result;
	
	@Column(name="source_airport")
	String srcAirport;

	public Long getScheduledId() {
		return scheduledId;
	}

	public void setScheduledId(Long scheduledId) {
		this.scheduledId = scheduledId;
	}

	public Date getArrDateTime() {
		return arrDateTime;
	}

	public void setArrDateTime(Date arrDateTime) {
		this.arrDateTime = arrDateTime;
	}

	public Date getDeptDateTime() {
		return deptDateTime;
	}

	public void setDeptDateTime(Date deptDateTime) {
		this.deptDateTime = deptDateTime;
	}

	public String getDstnAirport() {
		return dstnAirport;
	}

	public void setDstnAirport(String dstnAirport) {
		this.dstnAirport = dstnAirport;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public boolean isPrime() {
		return prime;
	}

	public void setPrime(boolean prime) {
		this.prime = prime;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getSrcAirport() {
		return srcAirport;
	}

	public void setSrcAirport(String srcAirport) {
		this.srcAirport = srcAirport;
	}

	
	

}
