package com.flightmanagementsystem.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="airport_table")
public class Airport {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="airport_code")
	long airportCode;
	
	@Column(name="airport_name")
	String aiportName;
	
	@Column(name="airport_location")
	String airportLocation;

	public long getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(long airportCode) {
		this.airportCode = airportCode;
	}

	public String getAiportName() {
		return aiportName;
	}

	public void setAiportName(String aiportName) {
		this.aiportName = aiportName;
	}

	public String getAirportLocation() {
		return airportLocation;
	}

	public void setAirportLocation(String airportLocation) {
		this.airportLocation = airportLocation;
	}

	
	
	
}
