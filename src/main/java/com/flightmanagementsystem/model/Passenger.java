package com.flightmanagementsystem.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name= "passenger_table")
public class Passenger {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="passenger_uid")
	Long passengerUIN;
	
	@Column(name="luggage")
	long luggage;
	
	@Column(name="passenger_age")
	int passengerAge;
	
	@Column(name="passenger_name")
	String passengerName;
	
	@Column(name="passenger_number")
	long passengerNumber;
}
