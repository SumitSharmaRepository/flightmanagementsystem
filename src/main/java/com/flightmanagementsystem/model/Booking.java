package com.flightmanagementsystem.model;

import java.sql.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name= "booking_table")
public class Booking {
	
	
	@Column(name="booking_date")
	Date bookingDate;
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="booking_id")
	long bookingId;
	
	@Column(name="number_of_passagenger")
	long numberOfPassenger;
	
	
}
